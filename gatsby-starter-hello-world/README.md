# Juste du feu

## Setting up
- Install Gatsby cli globally
- Install Firebase cli globally
- Install Yarn globally
- cd in project root, run ```yarn install```
- ```yarn develop```to run site on ```localhost:8000```

## Code structure
This project uses Gatsby but does not fetch data on build time. 
Each page has a css file, components use their page's css files.

## Deployement
Firebase hosting is used to deploy.
You will need to have firebase cli installed on your machine
Don't forget to run ```firebase login```
Two deployment instances are provided:
- [Production](www.justedufeu.com) ```yarn deploy-prod```
- [Stage](https://jdf-preview.web.app/) ```yarn deploy-stage```

Take a look a ```package.json```to use deploy commands. 
