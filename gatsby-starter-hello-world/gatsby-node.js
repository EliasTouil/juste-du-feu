const path = require("path")
exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions

  if (page.path.match(`/*\/collaborateurs/`)) {
    createPage({
      path: `/collaborateur`,
      matchPath: `/collaborateur/*`,
      component: path.resolve(`./src/pages/collaborateurs.js`),
    })
  }
  if (page.path.match(`/*\/projets/`)) {
    createPage({
      path: `/projet`,
      matchPath: `/projet/*`,
      component: path.resolve(`./src/pages/projets.js`),
    })
  }

}
