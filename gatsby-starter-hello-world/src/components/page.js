import React from "react"
import Nav from "./nav"

const Page = ({ children }) => {
  return (
    <>
      <Nav />
      {children}
    </>
  )
}

export default Page
